#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <wait.h>
#include <sys/unistd.h>
#include <sys/wait.h>
#include <pwd.h>
#include <grp.h>

pid_t pindah_air, pindah_darat, pindah_fish, pindah_bird, hapus_sisa, hapus_bird_belakang, hapus_bird_tengah, buat_list_file, pindah_list_air;
int sts;
FILE *fp;
DIR *dp;
struct dirent *entry;
char oks[50];
struct stat fs;
int r;
struct passwd *pw;

void masukkanlist(char di[]){
  //chdir(di);

  dp = opendir(di);
  if(dp != NULL){
    while ((entry = readdir(dp))){
      if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
      char tmp[50];
      char tmps[50];
      char uid[30];
     
      //strcpy(oks, entry->d_name);
      strcpy(tmp, di);
      strcat(tmp, "/");
      strcat(tmp, entry->d_name);
      r = stat(tmp, &fs);

      memset(tmp, 0, 10);
      strcpy(tmp, di);
      strcat(tmp, "/list.txt");
      // pw = getpwuid(fs.st_uid);
      sprintf(uid, "%s", getpwuid(fs.st_uid)->pw_name);
      strcpy(tmps, uid);
      strcat(tmps, "_");
      strcat(tmps, (fs.st_mode & S_IRUSR) ? "r" : "-");
      strcat(tmps, (fs.st_mode & S_IRUSR) ? "w" : "-");
      strcat(tmps, (fs.st_mode & S_IRUSR) ? "x" : "-");
      strcat(tmps, "_");
      strcat(tmps, entry->d_name);
      strcat(tmps, "\n");

      fp = fopen(tmp, "a");
      fwrite(tmps, 1, strlen(tmps), fp);
      fclose(fp);

    }
  }

}

int main()
{
  //Soal 3.a. Buat Folder darat dan air
  if (fork()==0) 
  {
    execlp("/bin/mkdir", "mkdir", "-p", "darat", NULL);
  }

  sleep(3);
  
  if (fork()==0) 
  {
    execlp("/bin/mkdir", "mkdir", "-p", "air", NULL);
  }
  
  //Soal 3.b Ekstrak Zip data hewan
  if (fork()==0)
  {
    execlp("/usr/bin/unzip", "unzip", "-oq","animal.zip", NULL);
  }
  
  sleep(2);

  //Soal 3.c Pemisahan hewan sesuai habitat
  while ((wait(&sts))>0);
  pindah_darat=fork();

  if(pindah_darat<0)
  {
    exit(EXIT_FAILURE);
  }

  if(pindah_darat==0)
  {
    execlp("/usr/bin/find", "find", "/home/nadim/modul2/animal", "-type", "f", "-name", "*darat.jpg", "-exec", "mv", "-t", "/home/nadim/modul2/darat", "{}", "+", (char*) NULL);
  }
  else
  {
    sleep(2);

    while ((wait(&sts))>0);
    pindah_air = fork();

    if(pindah_air<0)
    {
      exit(EXIT_FAILURE);
    }

    if(pindah_air==0)
    {
      execlp("/usr/bin/find", "find", "/home/nadim/modul2/animal", "-type", "f", "-name", "*air.jpg", "-exec", "mv", "-t", "/home/nadim/modul2/air", "{}", "+", (char*) NULL);
    }
  }

  sleep(2);

  while ((wait(&sts))>0);
  pindah_fish=fork();

  if (pindah_fish<0)
  {
    exit(EXIT_FAILURE);
  }

  if (pindah_fish==0)
  {
    execlp("/usr/bin/find", "find", "/home/nadim/modul2/animal", "-type", "f", "-name", "*air_fish.jpg", "-exec", "mv", "-t", "/home/nadim/modul2/air", "{}", "+", (char*) NULL); 
  }
  else
  {
    sleep(2);
    
    while ((wait(&sts))>0);
    pindah_bird=fork();

    if (pindah_bird<0)
    {
      exit(EXIT_FAILURE);
    }

    if (pindah_bird==0)
    {
      execlp("/usr/bin/find", "find", "/home/nadim/modul2/animal", "-type", "f", "-name", "*darat_bird.jpg", "-exec", "mv", "-t", "/home/nadim/modul2/darat", "{}", "+", (char*) NULL);
    }
  }

  sleep(2);
  
  //Hapus sisa hewan yang tidak masuk folder darat/air.
  while ((wait(&sts))>0);
  hapus_sisa=fork();

  if (hapus_sisa<0)
  {
    exit(EXIT_FAILURE);
  }

  if (hapus_sisa==0)
  {
    execlp("/usr/bin/find", "find", "/home/nadim/modul2/animal", "-type", "f", "-name", "*.jpg", "-exec", "rm", "-r", "{}", "+", (char*) NULL);
  }
    
  sleep(2);
  
  //Soal 3.d. Hapus bird dari folder darat.
  hapus_bird_belakang=fork();
  hapus_bird_tengah=fork();

  if (hapus_bird_belakang<0)
  {
    exit(EXIT_FAILURE);
  }

  if (hapus_bird_belakang==0)
  {
    execlp("/usr/bin/find", "find", "/home/nadim/modul2/darat", "-type", "f", "-name", "*bird.jpg", "-exec", "rm", "-r", "{}", "+", (char*) NULL);
  }

  if (hapus_bird_tengah<0)
  {
    exit(EXIT_FAILURE);
  }

  if (hapus_bird_tengah==0)
  {
    execlp("/usr/bin/find", "find", "/home/nadim/modul2/darat", "-type", "f", "-name", "*bird_darat.jpg", "-exec", "rm", "-r", "{}", "+", (char*) NULL);
  }

// Soal 3.e. Buat List File di Folder Air
  while ((wait(&sts))>0);
  buat_list_file=fork();

  if(buat_list_file<0)
  {
    exit(EXIT_FAILURE);
  }

  if(buat_list_file==0)
  {
    execlp("/bin/touch", "touch","list.txt", NULL);
  }

  while ((wait(&sts))>0);
  pindah_list_air=fork();

  if(pindah_list_air<0)
  {
    exit(EXIT_FAILURE);
  }

  if(pindah_list_air==0)
  {
    execlp("/usr/bin/find", "find", "/home/nadim/modul2", "-type", "f", "-name", "*list.txt", "-exec", "mv", "-t", "/home/nadim/modul2/air", "{}", "+", (char*) NULL);
  }

  while ((wait(&sts))>0);

  masukkanlist("air");

  exit(EXIT_SUCCESS);
}
